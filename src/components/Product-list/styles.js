import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    justify-content: center;
    background-color: #fff;
    border-radius: 10px;    
    box-shadow: 0 0 1px #D48A6A;     
`

export const List = styled.div`
    width: 300px;
    min-height: 150px;
    margin: 10px;
    

    li {
        display: flex;
        justify-content: space-between;
        margin: 5px;
    }
`