import styled from "styled-components";

export const Conteiner = styled.button`
    border: none;
    background-color: #AA5B39;
    border-radius: 5px;
    padding: 7px;
    font-size: 15px;
    color: #fff;
    cursor: pointer;

    :hover {
        background-color: #803515;
    }
`