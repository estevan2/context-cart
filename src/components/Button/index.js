import { useContext } from 'react'
import { cartContext } from '../../providers/cart'
import { CatalogueContext } from '../../providers/catalogue'
import { Conteiner } from './styles'

const Button = ({ type, item }) => {
    const { cart, addToCart, removeFromCart } = useContext(cartContext)
    const { catalogue, addToCatalogue, removeFromCatalogue } = useContext(CatalogueContext)

    const text = type === "catalogue" ? "Add to cart" : "Remove from cart"

    const handleClick = () => {
        if (type === "catalogue") {
            removeFromCatalogue(item)
            addToCart(item)
        } else {
            removeFromCart(item)
            addToCatalogue(item)
        }
    }

    return <Conteiner onClick={handleClick}>{text}</Conteiner>
}

export default Button