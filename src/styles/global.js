import { createGlobalStyle } from "styled-components";

export const GlobalStyles = createGlobalStyle`
    body {
        background-color: #ffc3aa29;
        font-family: Arial, Helvetica, sans-serif;
    }
`