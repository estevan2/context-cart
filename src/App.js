import ProductList from './components/Product-list'
import { CatalogueProvider } from './providers/catalogue'
import { CartProvider } from './providers/cart'
import { Container } from './styles'
import { GlobalStyles } from './styles/global'

function App() {
  return (
    <Container>
      <CatalogueProvider>
        <CartProvider>
          <GlobalStyles />
          <ProductList type="catalogue" />
          <ProductList type="cart" />
        </CartProvider>
      </CatalogueProvider>
    </Container>
  )
}

export default App;
